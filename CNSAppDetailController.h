@interface CNSAppDetailController : UITableViewController <UITableViewDelegate, UIAlertViewDelegate>
@property (nonatomic, retain) UISwitch *soundSwitch;
@property (nonatomic, retain) NSString *appTitle;
@property (nonatomic, retain) NSNumber *valueOfNotificationType;
@property (nonatomic, retain) id pusherController; // BulletinBoardAppDetailController
@property (nonatomic, retain) NSString *bundleIdentifier;
@property (nonatomic, retain) NSString *chosenSoundPath;
@property (nonatomic, retain) NSMutableArray *allSounds;
- (void)saveSettings;
- (void)initAllSounds;
- (void)like;
- (NSMutableArray *)allSections;
- (NSMutableArray *)soundsInSection:(NSInteger)sectionIndex;
- (void)setSoundsValue:(NSNumber *)value;
@end
